import React from 'react'
import ReactDOM from 'react-dom'

class App extends React.Component {

  render() {
    const reactVersion = require('./package.json').dependencies['react'];

    return ([
      <section className="text-center about">
      <br />
      <h1>About US</h1>
      <br />
      <hr />
      <br />
      <img src="./assets/logo.png" height="100px" />
      <br />
      <p style={{color: "black", margin: "20px 10vw", fontSize: "14px", textAlign: "justify", fontWeight: "600"}}>
      L'entreprise imaginaire MOUNACH CINEMA, est une entreprise qui a plusieurs locaux (cinémas), et qui a besoin d'automatiser le processus client, c'est-à-dire l'achat des tickets pour les sessions et aussi la gestion des cinémas par l’administrateur en ce qui concerne l’ajout, la modification et la suppression de tout ce qui concerne les cinémas, les salles et les sessions.
La plateforme MOUNACH CINEMA vise à satisfaire le besoin des clients en optimisant le temps consacré à l’achat des tickets. Ainsi, satisfaire le besoin de l’administrateur en optimisant le temps consacré à la gestion des cinémas, leurs théâtres et sessions. La plateforme MOUNACH CINEMA permettra d’éviter les actions manuelles et les automatiser en quelques clics.
L’application contient de différentes pages avec des fonctionnalité différentes, chaque page est dédiée à un processus d’utilisation spécifique, soit pour les clients, soit pour l’administrateur. La décision d’implémenter une telle application avec de différentes page a pour but de démontrer l’utilisation de l’approche de la modélisation autour des domaines d'activité (Domain-driven design) et de simuler de différentes équipes où chaque équipe est responsable des parties spécifiques de l’application.

      </p>
      <p>
          React Version: {reactVersion}
      </p>
    </section>
    ])
  }
}

class Mfe4Element extends HTMLElement {
  connectedCallback() {
    ReactDOM.render(<App/>, this);
  }
}

customElements.define('react-element', Mfe4Element);